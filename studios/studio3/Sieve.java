package studio3;

import cse131.ArgsProcessor;

public class Sieve {

	public static void main(String[] args) {
		ArgsProcessor ap = new ArgsProcessor(args);
		
		int size = ap.nextInt("How many elements in the array?");
		int[] arr = new int[size];
		
		for (int i=0;i<size;i++){
			arr[i]=i+2;
		}
		
		for (int i=2;i<size;i++){
			for (int j=i;j<size;j++){
				if (arr[j]%i == 0){
					arr[j] = 0;
				}
			}
		}
		
		for (int i=0;i<size;i++){
			if (arr[i] != 0){
				System.out.print(arr[i]+" ");
			}
		}

	}

}