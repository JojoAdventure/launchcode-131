package studio1;

import cse131.ArgsProcessor;

public class Average {

	public static void main(String[] args) {
		ArgsProcessor ap = new ArgsProcessor(args);
		double x = ap.nextDouble("First value!");
		double y = ap.nextDouble("Second value!");
		
		double avg = (x + y)/2;
		
		System.out.println("The average of these two numbers is "+avg+".");

	}

}
