package studio1;

import cse131.ArgsProcessor;

public class HiOne {
	
	public static void main(String[] args) {
		ArgsProcessor ap = new ArgsProcessor(args);
		String name = ap.nextString("What am name?");
		System.out.println("Hi, " + name + ", how are you?");
	}

}
