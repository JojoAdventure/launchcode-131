package studio1;

import cse131.ArgsProcessor;

public class HiFour {
	public static void main(String[] args) {
		ArgsProcessor ap = new ArgsProcessor(args);
		String s0 = ap.nextString("Indentify yourself, entity one!");
		String s1 = ap.nextString("Make yourself known, entity two!");
		String s2 = ap.nextString("Third guy. Name.");
		String s3 = ap.nextString("Come forth from the depths, entity four!");

		System.out.println("Good morrow, "+s0+", "+s1+", "+s2+", and "+s3+".");
		
	}
}
