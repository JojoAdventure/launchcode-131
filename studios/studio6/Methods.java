package studio6;

public class Methods {

	// Your methods go below this line
	public static int fact(int x){
		if(x==0 || x == 1) {return 1;}
		else {return x*fact(x-1);}
	}
	
	public static int fib(int x){
		if(x==0) {return 0;}
		else if (x==1){return 1;}
		else {return fib(x-1)+fib(x-2);}
	}
	
	public static boolean isOdd(int x){
		if(x==0) {return false;}
		else {return !isOdd(x-1);}
	}
	
	public static int sumDownBy2(int x){
		if(x==0) {return 0;}
		if(x==1) {return 1;}
		else {return x + sumDownBy2(x-2);}
	}
	
	public static double harmonicSum(double x){
		if (x==0) {return 0.0;}
		if (x==1) {return 1.0;}
		else {return (1/x)+(harmonicSum(x-1));}
	}
	
	public static int sum(int x, int y){
		if (x==0){return y;}
		if (y==0){return x;}
		else {return sum(x+1, y-1);}
	}
	
	public static int mult(int x, int y){
		if (x==0 || y==0){return 0;}
		else {return x + mult(x, y-1);}
	}
}
