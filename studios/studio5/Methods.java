package studio5;

public class Methods {
	
	/**
	 * Implemented correctly
	 * @param x one thing to add
	 * @param y the other thing to add
	 * @return the sum
	 */
	public static int sum(int x, int y) {
		return x+y;
	}
	
	/**
	 * 
	 * @param x one factor
	 * @param y another factor
	 * @return the product of the factors
	 */
	public static int mpy(int x, int y) {
		return 1;  // FIXME
	}
	
	/**
	 * 
	 * @param x First integer
	 * @param y Second integer
	 * @param z Third integer
	 * @return the average of the three integers as double
	 */
	public static double avg3(int x, int y, int z){
		double sum = x + y + z;
		double result = sum/3.0;
		return result;
	}
	
	/**
	 * 
	 * @param x Double array of two elements
	 * @return sum of two-element array
	 */
	public static double average(double[] x){
		double sum = (double)sum((int)x[0],(int)x[1]);
		double result = sum/2;
		return result;
	}
	
	/**
	 * 
	 * @param x Double array of [i] elements
	 * @return sum of all elements
	 */
	public static double sumArray(double[] x){
		double sum = 0;
		for (int i=0; i<x.length; i++){
			sum += x[i];
		}
		return sum;
	}
	
	/**
	 * 
	 * @param latin String input to be pig latin'd
	 * @return ethay ingstray (so long as first syllable is one consonant)
	 */
	public static String pig(String latin){
		String atin = latin.substring(1);
		String lay = latin.substring(0, 1) + "ay";
		String atinlay = atin + lay;
		return atinlay;
		
	}

	public static int Mod(int a, int b) {
		int rem = a;
		if (a >= b){
			while (rem >= b){
				rem -= b;
			}
		}
	return rem;
	}
}
