package studio2;

import cse131.ArgsProcessor;

public class RPS {

	public static void main(String[] args) {
		ArgsProcessor ap = new ArgsProcessor(args);

		double p1win=0;
		double p2win=0;
		
		int plays = ap.nextInt("How many games?");
		
		for (int i=1; i<=plays; i++){
			double p1 = Math.round(Math.random()*10)%3;				//0 is Rock, 1 is Paper, 2 is Scissors.
			double p2 = i%3;
			if (p1 == 0){
				if (p2 == 0){
					System.out.println("Tie");
				}
				else if (p2 == 1){
					p2win++;
					System.out.println("Player 2 wins: Paper beats Rock");
				}
				else {
					p1win++;
					System.out.println("Player 1 wins: Rock beats Scissors");
				}
			}
			else if (p1 == 1){
				if (p2 == 1){
					System.out.println("Tie");
				}
				else if (p2 == 2){
					p2win++;
					System.out.println("Player 2 wins: Scissors beats Paper");
				}
				else {
					p1win++;
					System.out.println("Player 1 wins: Paper beats Rock");
				}
			}
			if (p1 == 2){
				if (p2 == 2){
					System.out.println("Tie");
				}
				else if (p2 == 0){
					p2win++;
					System.out.println("Player 2 wins: Rock beats Scissors");
				}
				else {
					p1win++;
					System.out.println("Player 1 wins: Scissors beats Paper");
				}
			}
		}
		double total = p1win + p2win;
		double p1percent = ((int)((p1win/total)*1000))/10.0;
		double p2percent = ((int)((p2win/total)*1000))/10.0;
		
		System.out.println("\n Total games won: "+total+"\n Player 1 win rate: "+p1percent+"% \n Player 2 win rate: "+p2percent+"%");
	}
}
