package studio4;

import java.awt.Color;

import sedgewick.StdDraw;

public class Flag {

	public static void main(String[] args) {
		StdDraw.setPenColor(Color.BLUE);
		StdDraw.filledRectangle(0.5, 0.5, .5, .3);
		
		StdDraw.setPenRadius(.06);
		StdDraw.setPenColor(Color.BLACK);
		StdDraw.line(0.25, 0.21, 0.25, 0.59);
		StdDraw.setPenRadius(.04);
		StdDraw.setPenColor(Color.MAGENTA);
		StdDraw.line(0, 0.6, 1, 0.6);
		StdDraw.line(.2, 0.2, .2, 0.6);
		StdDraw.line(.3, 0.2, .3, 0.6);
		
		StdDraw.setPenRadius(0.05);
		StdDraw.setPenColor(Color.WHITE);
		StdDraw.rectangle(0.5, 0.5, .5, .3);
	}

}
