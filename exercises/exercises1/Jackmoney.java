package exercises1;

public class Jackmoney {

	public static void main(String[] args) {
		int jack = 2000;			//Jack had $20 when he went to the store
		int jill = 0;
		int franklin = 0;
		int sally = 0;
		
		jack = jack - 375;			//Jack bought an item for $3.75
		
		jack = jack - 523;
		jill = jill + 523;			//Jack gave $5.23 to Jill
		
		jill = jill - 140;			//Jill bought an item for $1.40
		
		jill = jill - 200;
		franklin = franklin + 200;	//Jill gave $2.00 to Franklin
		
		jack = jack - 405;
		franklin = franklin + 405;	//Jack gave $4.05 to Franklin
		
		sally = sally + (franklin/3);
		franklin = franklin - (franklin/3);	//Franklin gave 1/3 of his money to Sally
		
		double djack = jack/100.0;
		double djill = jill/100.0;
		double dfranklin = franklin/100.0;
		double dsally = sally/100.0;
		double total = djack + djill + dfranklin + dsally;
		
		System.out.println("Jack has $"+djack+" left.");
		System.out.println("Jill has $"+djill+" left.");
		System.out.println("Franklin has $"+dfranklin+" left.");
		System.out.println("Sally has $"+dsally+" now.");
		System.out.println("Total money left is $"+total+".");
	}

}
