package exercises1;

import cse131.ArgsProcessor;

public class Minutes {

	public static void main(String[] args) {
		ArgsProcessor ap = new ArgsProcessor(args);
		int min = ap.nextInt("How many minutes?:");		//Input number of minutes
		int hour = min/60;								//60 minutes per hour
		int day = hour/24;								//24 hours per day

		min = min % 60;									//Mod minutes by 60 to keep value within 60-minute limit	
		hour = hour % 24;									//Mod hours by 24 to keep value within 24-hour limit
		
		System.out.println("The value you gave is "+day+" days, "+hour+" hours, and "+min+" minutes.");
	}
}
