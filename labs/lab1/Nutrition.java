package lab1;
import cse131.ArgsProcessor;

public class Nutrition {

	public static void main(String[] args) {
		ArgsProcessor ap = new ArgsProcessor(args);
		String name = ap.nextString("What is the food?");
		double carbs = ap.nextDouble("How many grams of Carbs?");
		double fat = ap.nextDouble("How many grams of Fat?");
		double protein = ap.nextDouble("How many grams of Protein?");
		int statedCals = ap.nextInt("How many Calories are stated on the label?");
		
		double carbCal = ((int)((carbs*4)*100))/100.0;
		double fatCal = ((int)((fat*9)*100))/100.0;
		double proCal = ((int)((protein*4)*100))/100.0;
		
		double total = Math.round((carbCal + fatCal+ proCal)*100)/100.0;
		
		double perCarb = Math.round((carbCal/statedCals)*1000)/10.0;
		double perFat = Math.round((fatCal/statedCals)*1000)/10.0;
		double perPro = Math.round((proCal/statedCals)*1000)/10.0;

		double unusedCals = Math.round((total - statedCals)*10)/10.0;
		double fiber = ((int)((unusedCals/4)*100))/100.0;
		
		boolean carbAccept = perCarb < 25.0;
		boolean fatAccept = perFat < 15.0;
		boolean isEate = Math.random() > 0.5;
		
		System.out.println(name+" is said to have "+statedCals+" (avaliable) Calories.");
		System.out.println("With "+unusedCals+" unavailable Calories, this food has "+fiber+" grams of fiber.");
		System.out.println("");
		System.out.println("Approximately:");
		System.out.println(perCarb+"% of your food is carbohydrates");
		System.out.println(perFat+"% of your food is fat");
		System.out.println(perPro+"% of your food is protein");
		System.out.println("");
		System.out.println("This food is acceptable for a low-carb diet? "+carbAccept);
		System.out.println("This food is acceptable for a low-fat diet? "+fatAccept);
		System.out.println("By coin flip, you should eat this food? "+isEate);
		
	}

}
