package lab3;

import cse131.ArgsProcessor;

public class Dice {

	public static void main(String[] args) {
		ArgsProcessor ap = new ArgsProcessor(args);
		int dicenum = ap.nextInt("How many dice?");
		double thrownum = ap.nextInt("How many throws?");
		boolean isSame = true;
		double sumCount = 0;
		
		int[] diceThrown = new int[dicenum];
		
		for (int t=0; t<thrownum; t++){
			isSame = true;
			for(int d=0; d<dicenum; d++){
				int v = (int)(Math.random()*6)+1;
				diceThrown[d] = v;
			}
			for (int d=0; d<dicenum; d++){
				System.out.print(diceThrown[d]);
			}
			for (int d=0; d<dicenum; d++){
				if (diceThrown[d] == diceThrown[0]){
					if (d == (dicenum-1)){
						if (isSame == true){
							System.out.print(" They're the same!");
							sumCount++;
						}
					}
				}
				else if (diceThrown[d] != diceThrown[0]){
					isSame = false;
				}
			}
			System.out.println();
		}
		double sumRate = ((int)((sumCount/thrownum)*10000))/100.0;
		System.out.println();
		System.out.println("The percentage of throws that were identical is: "+sumRate+"%");
	}

}
