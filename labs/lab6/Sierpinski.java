package lab6;

import java.awt.Color;

import sedgewick.StdDraw;

public class Sierpinski {
	public static void sierpinski(double[] x, double[] y, int runs) {

		if(runs<1){return;}
		
		double[] wxpol = {(x[0] + x[2])/2, (x[0] + x[1])/2, (x[1] + x[2])/2};
		double[] wypol = {(y[0] + y[2])/2, (y[0] + y[1])/2, (y[1] + y[2])/2};
		StdDraw.setPenColor(Color.BLUE);
		StdDraw.filledPolygon(wxpol, wypol);
		
		double[] llxpol = {x[0], wxpol[1], wxpol[0]};
		double[] llypol = {y[0], wypol[1], wypol[0]};
		
		double[] lrxpol = {wxpol[1], x[1], wxpol[2]};
		double[] lrypol = {wypol[1], y[1], wypol[2]};
		
		double[] uxpol = {wxpol[0], wxpol[2], x[2]};
		double[] uypol = {wypol[0], wypol[2], y[2]};
		
		sierpinski(llxpol, llypol, runs-1);
		sierpinski(lrxpol, lrypol, runs-1);
		sierpinski(uxpol, uypol, runs-1);
	}
	
	public static void main(String[] args){
		double[] xpol = {0,1,.5};
		double[] ypol = {0,0,1};
		
		StdDraw.setPenColor(Color.BLACK);
		StdDraw.filledPolygon(xpol, ypol);
		
		sierpinski(xpol, ypol, 6);
		
	}
}
