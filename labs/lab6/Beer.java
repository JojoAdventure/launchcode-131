package lab6;

public class Beer {

	public static void main(String[] args) {
		System.out.println(beer(99));
	}
	
	public static String beer(int x){
		if (x==0){
			return "Wall is now empty! Time to drive home.";
		}
		else{
			if (x > 2){
				String bur = Integer.toString(x) + " bottles of beer on the wall, " + Integer.toString(x) + " bottles of beer, \nTake one down, pass it around, " + Integer.toString(x-1) + " bottles of beer on the wall. \n\n";
				return bur+beer(x-1);
			}
			else if (x == 2){
				String bur = Integer.toString(x) + " bottles of beer on the wall, " + Integer.toString(x) + " bottles of beer, \nTake one down, pass it around, " + Integer.toString(x-1) + " last bottle of beer on the wall. \n\n";
				return bur+beer(x-1);
			}
			else{
				String bur = Integer.toString(x) + " bottle of beer on the wall, " + Integer.toString(x) + " bottle of beer, \nTake it down, pass it around, no more bottles of beer on the wall. \n\n";
				return bur+beer(x-1);
			}
		}
	
	}

}
