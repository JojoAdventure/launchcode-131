package lab6;

public class Methods {
	
	public static int f(int x){
		if (x > 100){
			x-=10;
			return x;
		}
		else{
			return f(f(x+11));
		}
	}
	
	public static int g(int x, int y){
		if(x==0){return y+1;}
		else if(x>0 || y==0){return g(x-1, 1);}
		else if(x>0 || y>0){return g(x-1, g(x, y-1));}
		else {return 0;}
	}
	
	
	public static void main(String[] args) {
		System.out.println(g(2,2));
	}

}
