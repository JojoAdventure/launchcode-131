package lab2;

public class Pi {

	public static void main(String[] args) {
		double counter = 0;
		
		for (int i=100; i<1000000; i++){
			double x = Math.random();
			double y = Math.random();
			
			double difx = Math.abs(x-0.5);
			double dify = Math.abs(y-0.5);
			
			double dx2 = Math.pow(difx, 2);
			double dy2 = Math.pow(dify, 2);
			
			double sum = dx2+dy2;
			double dist = Math.sqrt(sum);
			
			if(dist <= 0.5){
				counter++;		
			}
		}
		double ratio = (counter/1000000.0)*4;
		System.out.println(ratio);
	}

}