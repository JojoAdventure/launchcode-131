package lab7;

public class Point {
	private double x;
	private double y;
	
	public Point(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}
	
	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public String toString(){
		return (new Point(3,-2)).toString();
	}
	
	public Point plus(Vector v){
		double newX = this.x + v.getDeltaX();
		double newY = this.y + v.getDeltaY();
		return new Point(newX, newY);
	}
	
	public Vector minus(Point p){
		double newX = this.x - p.x;
		double newY = this.y - p.y;
		return new Vector(newX, newY);
	}
	
	public double distance(Point p){
		return Math.sqrt(Math.pow(this.x - p.x, 2) + Math.pow(this.y - p.y, 2));
	}
}