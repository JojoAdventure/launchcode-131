package lab7;

public class Vector {
	private double deltaX;
	private double deltaY;
	
	public Vector(double deltaX, double deltaY) {
		super();
		this.deltaX = deltaX;
		this.deltaY = deltaY;
	}
	
	public String toString(){
		return (new Vector(4,3)).toString();
	}

	public double getDeltaX() {
		return deltaX;
	}

	public void setDeltaX(double deltaX) {
		this.deltaX = deltaX;
	}

	public double getDeltaY() {
		return deltaY;
	}

	public void setDeltaY(double deltaY) {
		this.deltaY = deltaY;
	}
	
	public double magnitude(){
		return (Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2)));
	}
	
	public Vector deflectX(){
		return new Vector(deltaX*(-1), deltaY);
	}
	
	public Vector deflectY(){
		return new Vector(deltaX, deltaY*(-1));
	}
	
	public Vector plus(Vector v){
		double xSum = v.deltaX + this.deltaX;
		double ySum = v.deltaY + this.deltaY;
		return new Vector(xSum, ySum);
	}
	
	public Vector minus(Vector v){
		double vX = v.getDeltaX();
		double vY = v.getDeltaY();
		double veX = this.getDeltaX();
		double veY = this.getDeltaY();
		double xDif = veX - vX;
		double yDif = veY - vY;
		return new Vector(xDif, yDif);
	}
	
	public Vector scale(double factor){
		double xFact = deltaX * factor;
		double yFact = deltaY * factor;
		return new Vector(xFact, yFact);
	}
	
	public Vector rescale(double magnitude){
		double mag = this.magnitude();
		if(mag == 0){
			return new Vector(magnitude, 0);
		}
		else {
			double factor = magnitude/mag;
			return scale(factor);
		}
	}
}
