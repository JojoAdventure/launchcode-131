package lab5;

public class Lab5Methods {
	public static int sumDownBy2(int x){
		int result=0;
		while (x >= 1){
			result += x;
			x -= 2;
		}
		return result;
	}

	public static double harmonicSum(int x){
		double result = 0;
		for (double i=1; i<=x; i++){
			result += 1/i;
		}
		return result;
	}
	
	public static double geometricSum(int x){
		double result = 0;
		x = Math.abs(x);
		for (double i=0; i<=x; i++){
			result += 1/Math.pow(2, i);
		}
		return result;
	}
	
	public static int multPos(int j, int k){
		int result = 0;
		for (int i=0; i<k; i++){
			result+=j;
		}
		return result;
	}
	
	public static int mult(int j, int k){
		int result = 0;
		if (j == 0 || k == 0){result = 0;}
		else if (j == 1 && k != 1){result = k;}
		else if (k == 1 && j != 1){result = j;}
		else if (j < 0 ^ k < 0){
			result -= multPos(Math.abs(j),Math.abs(k));
		}
		else {
			j = Math.abs(j);
			k = Math.abs(k);
			result = multPos(j,k);
		}
		return result;
	}
	
	public static int expt(int n, int k){
		int result = 1;
		for (int i=0; i<k; i++){
			result*=n;
		}
		return result;
	}
}