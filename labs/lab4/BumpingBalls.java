package lab4;

import cse131.ArgsProcessor;
import sedgewick.StdDraw;

public class BumpingBalls {
	
	public static void main(String[] args) {
		ArgsProcessor ap = new ArgsProcessor(args);
		
		StdDraw.setXscale(-1.0,1.0);
		StdDraw.setYscale(-1.0,1.0);
		
		int buals = ap.nextInt("How many buals?");
		double radius = .05;
		
		double[] xvel = new double[buals];
		double[] yvel = new double[buals];
		double[] xcoor = new double[buals];
		double[] ycoor = new double[buals];
		
		for (int i=0; i<buals; i++){
			xvel[i] = Math.random()/20;
			yvel[i] = Math.random()/20;
			xcoor[i] = Math.random();
			ycoor[i] = Math.random();
		}
		
		while (true){
			StdDraw.clear();
			for (int i=0; i<buals; i++){
				if (Math.abs(xcoor[i] + xvel[i]) > 1.0 - radius) xvel[i] = -xvel[i];
				if (Math.abs(ycoor[i] + yvel[i]) > 1.0 - radius) yvel[i] = -yvel[i];

				// update position
				xcoor[i] = xcoor[i] + xvel[i]; 
				ycoor[i] = ycoor[i] + yvel[i]; 
            
				//draw buals
				StdDraw.setPenColor(StdDraw.BLUE); 
				StdDraw.filledCircle(xcoor[i], ycoor[i], radius);
			}
			
			for (int i=0; i<buals; i++){
				for (int j=1+i; j<buals; j++){
					double xdist = Math.abs(xcoor[i] - xcoor[j]);
					double ydist = Math.abs(ycoor[i] - ycoor[j]);
					double xdist2 = Math.pow(xdist, 2);
					double ydist2 = Math.pow(ydist, 2);
					double sumxy = xdist2 + ydist2;
					double distance = Math.sqrt(sumxy);
					
					if (distance < radius*2){
						xvel[i] = -xvel[i];
						yvel[i] = -yvel[i];
						xvel[j] = -xvel[j];
						yvel[j] = -yvel[j];
					}
				}
			}
			StdDraw.show(20);
		}
		
	}

}
